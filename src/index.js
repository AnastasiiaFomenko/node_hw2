require('dotenv').config()
const port = process.env.DB_PORT || 8080;
const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');

const {notesController} = require('./controllers/noteController');
const {authController} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {usersController} = require('./controllers/usersController');

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authController);

app.use(authMiddleware);
app.use('/api/notes', notesController);
app.use('/api/users/me', usersController);

app.get('/', (req, res) => {
  res.status(200).json({'message': 'success'});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://Af13:ZYSq56FPzQ6hnsJ@cluster0.tybje.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });

    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use((err, req, res, next) => {
  res.status(500).send({ message: 'Server error' });
});
