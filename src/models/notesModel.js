const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://Af13:ZYSq56FPzQ6hnsJ@cluster0.tybje.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
  useNewUrlParser: true, useUnifiedTopology: true,
});

const Note = mongoose.model('note', {
  titel: {
    type: String,
  },
  text: {
    required: true,
    type: String,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Note};
