const {User} = require('../models/userModel');

const getUserProfileById = async (userId) => {
  const profile = await User.findById(userId, '-__v -password');
  return profile;
};

const deleteUserProfileById = async (userId) => {
  await User.findOneAndRemove(userId);
};

module.exports = {
  getUserProfileById,
  deleteUserProfileById,
};
