const {Note} = require('../models/notesModel');

const getNotesByUserId = async (userId) => {
  const notes = await Note.find({userId});
  return notes;
};

const getNoteById = async (noteId) => {
  const note = await Note.findById(noteId);
  return note;
};

const updateNoteById = async (noteId, newText) => {
  const note = await Note.findOneAndUpdate(noteId, {$set: {text: newText}});
  return note;
};

const addNotesToUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
  return note;
};

const checkNoteById = async (noteId) => {
  const cheked = await Note.findById(noteId).completed;
  const note = await Note.findOneAndUpdate(noteId, {$set: {
    completed: !cheked}});
  return note;
};

const deleteNoteById = async (noteId) => {
  await Note.findOneAndRemove(noteId);
};


module.exports = {
  getNotesByUserId,
  getNoteById,
  addNotesToUser,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
};
