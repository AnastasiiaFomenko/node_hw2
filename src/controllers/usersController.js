const express = require('express');
const router = new express.Router();

const {
  getUserProfileById,
  deleteUserProfileById,
} = require('../services/usersService');

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const profile = await getUserProfileById(userId);
    res.status(200).json({user: profile});
  } catch (err) {
    res.status(400).json({message: 'bed request'});
  }
});

router.delete('/', async (req, res) => {
  try {
    const {userId} = req.user;
    await deleteUserProfileById(userId);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'bed request'});
  }
});

module.exports = {
  usersController: router,
};
