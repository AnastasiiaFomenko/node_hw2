const express = require('express');
const router = new express.Router();

const {getNotesByUserId,
  getNoteById,
  addNotesToUser,
  updateNoteById,
  checkNoteById,
  deleteNoteById} = require('../services/notesService');

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const notes = await getNotesByUserId(userId);
    res.status(200).json({
      offset: 0,
      limit: 0,
      count: 0,
      notes: notes});
  } catch (err) {
    res.status(400).json({message: 'Notes not found'});
  }
});


router.get('/:id', async (req, res) => {
  try {
    const note = await getNoteById(req.params.id);
    res.status(200).json({note});
  } catch (err) {
    res.status(400).json({message: 'Note not found'});
  }
});

router.post('/', async (req, res) => {
  try {
    const {userId} = req.user;
    await addNotesToUser(userId, req.body);
    res.status(200).json({message: 'Note created successfully'});
  } catch (err) {
    res.status(400).json({message: 'Fail note creating'});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {text} = req.body;
    await updateNoteById(req.params.id, text);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'Note not found'});
  }
});

router.patch('/:id', async (req, res) => {
  try {
    await checkNoteById(req.params.id);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'Note not found'});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await deleteNoteById(req.params.id);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: 'Note not found'});
  }
});

module.exports = {
  notesController: router,
};
