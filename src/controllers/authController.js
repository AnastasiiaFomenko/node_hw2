const express = require('express');
const router = new express.Router();

const {registration,
  logIn} = require('../services/authService');


router.post('/register', async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;

    await registration({username, password});

    res.status(200).json({message: 'Account created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/login', async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;

    const token = await logIn({username, password});
    res.status(200).json({message: 'Logged in succcessfully', jwt_token: token});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = {
  authController: router,
};
